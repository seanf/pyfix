import setuptools

from pyfix import __version__

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setuptools.setup(
    name="pyfix",
    version=__version__,
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Python implementation of FSL's FIX tool for denoising",
    url="https://git.fmrib.ox.ac.uk/seanf/pyfix",
    install_requires=install_requires,
    scripts=['pyfix/fix'],
    # packages=setuptools.find_packages(),
    packages=['pyfix'],
    package_dir={'pyfix': 'pyfix'},
    package_data={'pyfix': ['resources/*', 'resources/masks/*']},
    include_package_data=True,
    python_requires='>=3.8',
)